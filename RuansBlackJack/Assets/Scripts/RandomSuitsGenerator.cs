﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSuitsGenerator : MonoBehaviour
{
    public GameObject clubs;
    public GameObject diamonds;
    public GameObject hearts;
    public GameObject spades;
    
    int randomInt;

    void Start()
    {
        randomInt = Random.Range(1, 5);

        if(randomInt == 1)
        {
            clubs.SetActive(true);
        }
        if(randomInt == 2)
        {
            diamonds.SetActive(true);
        }
        if(randomInt == 3)
        {
            hearts.SetActive(true);
        }
        if(randomInt >= 4)
        {
            spades.SetActive(true);
        }
    }
    
}
