﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public void ExitGame()
    {
        Application.Quit();
    }

    public void ToGame()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
