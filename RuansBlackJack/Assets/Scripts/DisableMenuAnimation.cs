﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMenuAnimation : MonoBehaviour
{
    public GameObject leftPanel;
    public GameObject rightPanel;

    void Start()
    {
        StartCoroutine("StopAnimations");
    }

    IEnumerator StopAnimations()
    {
        yield return new WaitForSeconds(1f);

        leftPanel.SetActive(false);
        rightPanel.SetActive(false);
    }
}
