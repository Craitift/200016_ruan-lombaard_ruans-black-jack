﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerprefsManager : MonoBehaviour
{
    public int moneyAmount = 200;
    public int winCount = 0;
    public int loseCount = 0;

    public GameObject winScreen;
    public GameObject loseScreen;
    public GameObject standOffScreen;

    [SerializeField] int playerScore = 0;
    [SerializeField] int dealerScore = 0;

    int randomInt;
    int dealerRandomInt;
    int betAmount = 0;
    int winAmount = 0;

    public Text moneyText;

    public Text betAmountText;
    public Text winAmountText;
    
    public Text winsText;

    public Text lossesText;

    public Text playerScoreText;
    public Text dealerScoreText;

    public GameObject betButtons;

    public GameObject betTwentyButton;
    public GameObject betFiftyButton;
    public GameObject betHundredButton;
    public GameObject betTwoHundredButton;

    public GameObject[] randomCards;

    public GameObject blackJackWin;
    public GameObject bustLose;
    public GameObject blackJackLose;
    public GameObject bustWin;

    public Transform spawnPos;
    public GameObject positionOne;
    public GameObject positionTwo;
    public GameObject positionThree;
    public GameObject positionFour;

    public Transform dealerSpawnPos;
    public GameObject dealerPositionOne;
    public GameObject dealerPositionTwo;
    public GameObject dealerPositionThree;
    public GameObject dealerPositionFour;

    public GameObject cardOne;
    public GameObject cardTwo;
    public GameObject cardThree;
    public GameObject cardFour;

    public GameObject dealerCardOne;
    public GameObject dealerCardTwo;
    public GameObject dealerCardThree;
    public GameObject dealerCardFour;

    void Start()
    {
        // Load player money, wins and losses.
        moneyAmount = PlayerPrefs.GetInt("playerMoney");
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();

        winCount = PlayerPrefs.GetInt("playerWins");
        winsText.GetComponent<UnityEngine.UI.Text>().text = winCount.ToString();

        loseCount = PlayerPrefs.GetInt("playerLosses");
        lossesText.GetComponent<UnityEngine.UI.Text>().text = loseCount.ToString();

        if(moneyAmount <= 0)
        {
            moneyAmount = 200;
            PlayerPrefs.SetInt("playerMoney", moneyAmount);
            moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();

            winCount = 0;
            PlayerPrefs.SetInt("playerWins", winCount);
            winsText.GetComponent<UnityEngine.UI.Text>().text = winCount.ToString();

            loseCount = 0;
            PlayerPrefs.SetInt("playerLosses", loseCount);
            lossesText.GetComponent<UnityEngine.UI.Text>().text = loseCount.ToString();

            ReloadScene();
        }

        if(moneyAmount < 20)
        {
            betTwentyButton.SetActive(false);
        }
        if(moneyAmount < 50)
        {
            betFiftyButton.SetActive(false);
        }
        if(moneyAmount < 100)
        {
            betHundredButton.SetActive(false);
        }
        if(moneyAmount < 200)
        {
            betTwoHundredButton.SetActive(false);
        }
    }

    public void DrawCardOne()
    {
        randomInt = Random.Range(0, randomCards.Length);
        spawnPos.position = positionOne.transform.position;
        //spawnPos.transform.rotation = positionOne.transform.rotation;
        Instantiate (randomCards[randomInt], spawnPos.position, spawnPos.rotation);
        if(randomInt == 10)
        {
            playerScore = playerScore + 10;
        }
        else
        {
            if(randomInt == 11)
            {
                playerScore = playerScore + 10;
            }
            else
            {
                if(randomInt == 12)
                {
                    playerScore = playerScore + 10;
                }
                else
                {
                    playerScore = playerScore + randomInt + 1;
                }
            }
        }

        if(randomInt == 0 && playerScore < 10)
        {
            playerScore = playerScore + 10;
        }
        if(playerScore == 21)
        {
            AddBlackJackWin();
        }
        if(playerScore > 21)
        {
            AddBustLoss();
        }
        cardOne.SetActive(false);
        playerScoreText.GetComponent<UnityEngine.UI.Text>().text = playerScore.ToString();
    }
    public void DrawCardTwo()
    {
        randomInt = Random.Range(0, randomCards.Length);
        spawnPos.position = positionTwo.transform.position;
        //spawnPos.transform.rotation = positionTwo.transform.rotation;
        Instantiate (randomCards[randomInt], spawnPos.position, spawnPos.rotation);
        if(randomInt == 10)
        {
            playerScore = playerScore + 10;
        }
        else
        {
            if(randomInt == 11)
            {
                playerScore = playerScore + 10;
            }
            else
            {
                if(randomInt == 12)
                {
                    playerScore = playerScore + 10;
                }
                else
                {
                    playerScore = playerScore + randomInt + 1;
                }
            }
        }

        if(randomInt == 0 && playerScore < 10)
        {
            playerScore = playerScore + 10;
        }
        if(randomInt == 0 && playerScore == 11)
        {
            AddBlackJackWin();
        }
        if(playerScore == 21)
        {
            AddBlackJackWin();
        }
        if(playerScore > 21)
        {
            AddBustLoss();
        }
        cardTwo.SetActive(false);
        playerScoreText.GetComponent<UnityEngine.UI.Text>().text = playerScore.ToString();
    }
    public void DrawCardThree()
    {
        randomInt = Random.Range(0, randomCards.Length);
        spawnPos.position = positionThree.transform.position;
        //spawnPos.transform.rotation = positionThree.transform.rotation;
        Instantiate (randomCards[randomInt], spawnPos.position, spawnPos.rotation);
        if(randomInt == 10)
        {
            playerScore = playerScore + 10;
        }
        else
        {
            if(randomInt == 11)
            {
                playerScore = playerScore + 10;
            }
            else
            {
                if(randomInt == 12)
                {
                    playerScore = playerScore + 10;
                }
                else
                {
                    playerScore = playerScore + randomInt + 1;
                }
            }
        }

        if(randomInt == 0 && playerScore < 10)
        {
            playerScore = playerScore + 10;
        }
        if(playerScore > 21)
        {
            AddBustLoss();
        }
        cardThree.SetActive(false);
        playerScoreText.GetComponent<UnityEngine.UI.Text>().text = playerScore.ToString();
    }
    public void DrawCardFour()
    {
        randomInt = Random.Range(0, randomCards.Length);
        spawnPos.position = positionFour.transform.position;
        //spawnPos.transform.rotation = positionFour.transform.rotation;
        Instantiate (randomCards[randomInt], spawnPos.position, spawnPos.rotation);
        if(randomInt == 10)
        {
            playerScore = playerScore + 10;
        }
        else
        {
            if(randomInt == 11)
            {
                playerScore = playerScore + 10;
            }
            else
            {
                if(randomInt == 12)
                {
                    playerScore = playerScore + 10;
                }
                else
                {
                    playerScore = playerScore + randomInt + 1;
                }
            }
        }

        if(randomInt == 0 && playerScore < 10)
        {
            playerScore = playerScore + 10;
        }
        if(playerScore > 21)
        {
            AddBustLoss();
        }
        cardFour.SetActive(false);
        playerScoreText.GetComponent<UnityEngine.UI.Text>().text = playerScore.ToString();
    }

    public void Bet10()
    {
        moneyAmount = moneyAmount - 10;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);

        betAmount = 10;
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet20()
    {
        moneyAmount = moneyAmount - 20;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);

        betAmount = 20;
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet50()
    {
        moneyAmount = moneyAmount - 50;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);

        betAmount = 50;
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet100()
    {
        moneyAmount = moneyAmount - 100;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);

        betAmount = 100;
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet200()
    {
        moneyAmount = moneyAmount - 200;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);

        betAmount = 200;
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void BetAll()
    {
        betAmount = moneyAmount;
        moneyAmount = 0;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet25Percent()
    {
        betAmount = moneyAmount / 4;
        moneyAmount = moneyAmount - betAmount;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet50Percent()
    {
        betAmount = moneyAmount / 2;
        moneyAmount = moneyAmount - betAmount;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    public void Bet75Percent()
    {
        betAmount = (moneyAmount / 4) + (moneyAmount / 4) + (moneyAmount / 4);
        moneyAmount = moneyAmount - betAmount;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
        betButtons.SetActive(false);
        betAmountText.GetComponent<UnityEngine.UI.Text>().text = "(Bet)  - $ " + betAmount.ToString();

        winAmount = betAmount * 2;
        winAmountText.GetComponent<UnityEngine.UI.Text>().text = "+ $ " + winAmount.ToString();
    }

    void AddWin()
    {
        winCount = winCount + 1;
        PlayerPrefs.SetInt("playerWins", winCount);
        winsText.GetComponent<UnityEngine.UI.Text>().text = winCount.ToString();

        moneyAmount = moneyAmount + betAmount + betAmount;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
    }

    void AddBlackJackWin()
    {
        winCount = winCount + 1;
        PlayerPrefs.SetInt("playerWins", winCount);
        winsText.GetComponent<UnityEngine.UI.Text>().text = winCount.ToString();

        blackJackWin.SetActive(true);
        StartCoroutine("BlackJackAnimation");

        moneyAmount = moneyAmount + betAmount + betAmount + betAmount;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
    }

    void AddLoss()
    {
        loseCount = loseCount + 1;
        PlayerPrefs.SetInt("playerLosses", loseCount);
        lossesText.GetComponent<UnityEngine.UI.Text>().text = loseCount.ToString();
    }

    void AddBustLoss()
    {
        loseCount = loseCount + 1;
        PlayerPrefs.SetInt("playerLosses", loseCount);
        lossesText.GetComponent<UnityEngine.UI.Text>().text = loseCount.ToString();
        bustLose.SetActive(true);
        StartCoroutine("BustAnimation");
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void AddTwoHundredFunds()
    {
        moneyAmount = moneyAmount + 200;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
    }

    IEnumerator BlackJackAnimation()
    {
        yield return new WaitForSeconds(3f);

        if(moneyAmount <= 0)
        {
           SceneManager.LoadScene("MainMenu");
        }
        else
        {
            ReloadScene();
        }
    }

    IEnumerator BustAnimation()
    {
        yield return new WaitForSeconds(3f);

        if(moneyAmount <= 0)
        {
           SceneManager.LoadScene("MainMenu");
        }
        else
        {
            ReloadScene();
        }
    }

    //----------------------------------------------------------------------------------------
    // Dealer Code
    //----------------------------------------------------------------------------------------

    public void DrawDealerCardOne()
    {
        dealerRandomInt = Random.Range(0, randomCards.Length);
        dealerSpawnPos.position = dealerPositionOne.transform.position;
        Instantiate (randomCards[dealerRandomInt], dealerSpawnPos.position, dealerSpawnPos.rotation);
        if(dealerRandomInt == 10)
        {
            dealerScore = dealerScore + 10;
        }
        else
        {
            if(dealerRandomInt == 11)
            {
                dealerScore = dealerScore + 10;
            }
            else
            {
                if(dealerRandomInt == 12)
                {
                    dealerScore = dealerScore + 10;
                }
                else
                {
                    dealerScore = dealerScore + dealerRandomInt + 1;
                }
            }
        }

        if(dealerRandomInt == 0 && dealerScore < 10)
        {
            dealerScore = dealerScore + 10;
        }
        if(dealerScore == 21)
        {
            AddBlackJackLoss();
        }
        if(dealerScore > 21)
        {
            AddBustWin();
        }
        dealerCardOne.SetActive(false);
        dealerScoreText.GetComponent<UnityEngine.UI.Text>().text = dealerScore.ToString();
    }
    public void DrawDealerCardTwo()
    {
        dealerRandomInt = Random.Range(0, randomCards.Length);
        dealerSpawnPos.position = dealerPositionTwo.transform.position;
        Instantiate (randomCards[dealerRandomInt], dealerSpawnPos.position, dealerSpawnPos.rotation);
        if(dealerRandomInt == 10)
        {
            dealerScore = dealerScore + 10;
        }
        else
        {
            if(dealerRandomInt == 11)
            {
                dealerScore = dealerScore + 10;
            }
            else
            {
                if(dealerRandomInt == 12)
                {
                    dealerScore = dealerScore + 10;
                }
                else
                {
                    dealerScore = dealerScore + dealerRandomInt + 1;
                }
            }
        }

        if(dealerRandomInt == 0 && dealerScore < 10)
        {
            dealerScore = dealerScore + 10;
        }
        if(dealerRandomInt == 0 && dealerScore == 11)
        {
            AddBlackJackLoss();
        }
        if(dealerScore == 21)
        {
            AddBlackJackLoss();
        }
        if(dealerScore > 21)
        {
            AddBustWin();
        }
        dealerCardTwo.SetActive(false);
        dealerScoreText.GetComponent<UnityEngine.UI.Text>().text = dealerScore.ToString();
    }

    public void DrawDealerCardThree()
    {
        dealerRandomInt = Random.Range(0, randomCards.Length);
        dealerSpawnPos.position = dealerPositionThree.transform.position;
        Instantiate (randomCards[dealerRandomInt], dealerSpawnPos.position, dealerSpawnPos.rotation);
        if(dealerRandomInt == 10)
        {
            dealerScore = dealerScore + 10;
        }
        else
        {
            if(dealerRandomInt == 11)
            {
                dealerScore = dealerScore + 10;
            }
            else
            {
                if(dealerRandomInt == 12)
                {
                    dealerScore = dealerScore + 10;
                }
                else
                {
                    dealerScore = dealerScore + dealerRandomInt + 1;
                }
            }
        }

        if(dealerRandomInt == 0 && dealerScore < 10)
        {
            dealerScore = dealerScore + 10;
        }
        if(dealerScore > 21)
        {
            AddBustWin();
        }
        dealerCardThree.SetActive(false);
        dealerScoreText.GetComponent<UnityEngine.UI.Text>().text = dealerScore.ToString();
    }
    public void DrawDealerCardFour()
    {
        dealerRandomInt = Random.Range(0, randomCards.Length);
        dealerSpawnPos.position = dealerPositionFour.transform.position;
        Instantiate (randomCards[dealerRandomInt], dealerSpawnPos.position, dealerSpawnPos.rotation);
        if(dealerRandomInt == 10)
        {
            dealerScore = dealerScore + 10;
        }
        else
        {
            if(dealerRandomInt == 11)
            {
                dealerScore = dealerScore + 10;
            }
            else
            {
                if(dealerRandomInt == 12)
                {
                    dealerScore = dealerScore + 10;
                }
                else
                {
                    dealerScore = dealerScore + dealerRandomInt + 1;
                }
            }
        }

        if(dealerRandomInt == 0 && dealerScore < 10)
        {
            dealerScore = dealerScore + 10;
        }
        if(dealerScore > 21)
        {
            AddBustWin();
        }
        dealerCardFour.SetActive(false);
        dealerScoreText.GetComponent<UnityEngine.UI.Text>().text = dealerScore.ToString();
    }

    void AddBustWin()
    {
        winCount = winCount + 1;
        PlayerPrefs.SetInt("playerWins", winCount);
        winsText.GetComponent<UnityEngine.UI.Text>().text = winCount.ToString();

        bustWin.SetActive(true);
        StartCoroutine("BustAnimation");

        moneyAmount = moneyAmount + betAmount + betAmount;
        PlayerPrefs.SetInt("playerMoney", moneyAmount);
        moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
    }

    void AddBlackJackLoss()
    {
        loseCount = loseCount + 1;
        PlayerPrefs.SetInt("playerLosses", loseCount);
        lossesText.GetComponent<UnityEngine.UI.Text>().text = loseCount.ToString();
        blackJackLose.SetActive(true);
        StartCoroutine("BlackJackAnimation");
    }

    public void TestForDealerDrawTwo()
    {
        if(dealerScore < 17)
        {
            DrawDealerCardTwo();
        }
        else
        {
            if(playerScore > dealerScore)
            {
                winScreen.SetActive(true);
                AddWin();
                StartCoroutine("BlackJackAnimation");
            }
            if(playerScore < dealerScore)
            {
                loseScreen.SetActive(true);
                AddLoss();
                StartCoroutine("BustAnimation");
            }
            if(playerScore == dealerScore)
            {
                standOffScreen.SetActive(true);
                moneyAmount = moneyAmount + betAmount;
                PlayerPrefs.SetInt("playerMoney", moneyAmount);
                moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
                StartCoroutine("BustAnimation");
            }
        }
    }

    public void TestForDealerDrawThree()
    {
        if(dealerScore < 17)
        {
            DrawDealerCardThree();
        }
        else
        {
            if(playerScore > dealerScore)
            {
                winScreen.SetActive(true);
                AddWin();
                StartCoroutine("BlackJackAnimation");
            }
            if(playerScore < dealerScore)
            {
                loseScreen.SetActive(true);
                AddLoss();
                StartCoroutine("BustAnimation");
            }
            if(playerScore == dealerScore)
            {
                standOffScreen.SetActive(true);
                moneyAmount = moneyAmount + betAmount;
                PlayerPrefs.SetInt("playerMoney", moneyAmount);
                moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
                StartCoroutine("BustAnimation");
            }
        }
    }

    public void TestForDealerDrawFour()
    {
        if(dealerScore < 17)
        {
            DrawDealerCardFour();
        }
        else
        {
            if(playerScore > dealerScore)
            {
                winScreen.SetActive(true);
                AddWin();
                StartCoroutine("BlackJackAnimation");
            }
            if(playerScore < dealerScore)
            {
                loseScreen.SetActive(true);
                AddLoss();
                StartCoroutine("BustAnimation");
            }
            if(playerScore == dealerScore)
            {
                standOffScreen.SetActive(true);
                moneyAmount = moneyAmount + betAmount;
                PlayerPrefs.SetInt("playerMoney", moneyAmount);
                moneyText.GetComponent<UnityEngine.UI.Text>().text = "$ " + moneyAmount.ToString();
                StartCoroutine("BustAnimation");
            }
        }
    }
}
