﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : MonoBehaviour
{
    public GameObject redVelvet;
    public GameObject blueVelvet;
    public GameObject greenVelvet;
    public GameObject purpleVelvet;
    public GameObject wood;
    public GameObject redWood;
    public GameObject greenWood;
    public GameObject redJapaneseWave;
    public GameObject blueJapaneseWave;
    public GameObject greenJapaneseWave;
    public GameObject lightMetal;
    public GameObject darkMetal;
    public GameObject gold;

    [SerializeField] int activeTable = 0;

    void Start()
    {
        activeTable = PlayerPrefs.GetInt("SelectedTable");

        if(activeTable == 0)
        {
            redVelvet.SetActive(true);
        }
        if(activeTable == 1)
        {
            blueVelvet.SetActive(true);
        }
        if(activeTable == 2)
        {
            greenVelvet.SetActive(true);
        }
        if(activeTable == 3)
        {
            purpleVelvet.SetActive(true);
        }
        if(activeTable == 4)
        {
            wood.SetActive(true);
        }
        if(activeTable == 5)
        {
            redWood.SetActive(true);
        }
        if(activeTable == 6)
        {
            greenWood.SetActive(true);
        }
        if(activeTable == 7)
        {
            redJapaneseWave.SetActive(true);
        }
        if(activeTable == 8)
        {
            blueJapaneseWave.SetActive(true);
        }
        if(activeTable == 9)
        {
            greenJapaneseWave.SetActive(true);
        }
        if(activeTable == 10)
        {
            lightMetal.SetActive(true);
        }
        if(activeTable == 11)
        {
            darkMetal.SetActive(true);
        }
        if(activeTable == 12)
        {
            gold.SetActive(true);
        }
    }

    public void SelectRedVelvet()
    {
        PlayerPrefs.SetInt("SelectedTable", 0);
    }

    public void SelectBlueVelvet()
    {
        PlayerPrefs.SetInt("SelectedTable", 1);
    }

    public void SelectGreenVelvet()
    {
        PlayerPrefs.SetInt("SelectedTable", 2);
    }

    public void SelectPurpleVelvet()
    {
        PlayerPrefs.SetInt("SelectedTable", 3);
    }

    public void SelectWood()
    {
        PlayerPrefs.SetInt("SelectedTable", 4);
    }

    public void SelectRedWood()
    {
        PlayerPrefs.SetInt("SelectedTable", 5);
    }

    public void SelectGreenWood()
    {
        PlayerPrefs.SetInt("SelectedTable", 6);
    }

    public void SelectRedJapan()
    {
        PlayerPrefs.SetInt("SelectedTable", 7);
    }

    public void SelectBlueJapan()
    {
        PlayerPrefs.SetInt("SelectedTable", 8);
    }

    public void SelectGreenJapan()
    {
        PlayerPrefs.SetInt("SelectedTable", 9);
    }

    public void SelectLightMetal()
    {
        PlayerPrefs.SetInt("SelectedTable", 10);
    }

    public void SelectDarkMetal()
    {
        PlayerPrefs.SetInt("SelectedTable", 11);
    }

    public void SelectGold()
    {
        PlayerPrefs.SetInt("SelectedTable", 12);
    }
}
